//
// Created by Kostya on 08.12.2022.
//

#include "tester.h"

#define HEAP_SIZE 5000
#define BLOCK_SIZE 500
#define message(message, name) printf("Test: %s. \"%s\"", name, message);

static void _free_heap(void* heap) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
}

static struct block_header* get_header_by_pointer(void* pointer) {
    return (struct block_header*) ((uint8_t*) pointer) - offsetof(struct block_header, contents);
}

void test_1() {
    const char* name = "Simple allocation";

    message("started", name)

    void *heap = heap_init(HEAP_SIZE);
    message("heap initialized", name)

    void *try_malloc = _malloc(BLOCK_SIZE);
    message("try to initialize block", name);

    debug_heap(stdout, heap);

    if (try_malloc == NULL || get_header_by_pointer(try_malloc)->capacity.bytes != BLOCK_SIZE) {
        message("failed", name)
        _free(try_malloc);
        _free_heap(heap);
        return;
    }

    message("success", name)

    _free(try_malloc);
    _free_heap(heap);

    message("ended", name)
}

void test_2() {
    const char* name = "Huge memory allocation";

    message("started", name)

    void *heap = heap_init(HEAP_SIZE);
    message("heap initialized", name)

    void *block_1 = _malloc(BLOCK_SIZE);
    message("malloc block_1", name)
    void *block_2 = _malloc(BLOCK_SIZE);
    message("malloc block_2", name)
    void *block_3 = _malloc(BLOCK_SIZE);
    message("malloc block_3", name)
    void *block_4 = _malloc(BLOCK_SIZE);
    message("malloc block_4", name)
    void *block_5 = _malloc(BLOCK_SIZE);
    message("malloc block_5", name)
    void *block_6 = _malloc(BLOCK_SIZE);
    message("malloc block_6", name)
    void *block_7 = _malloc(BLOCK_SIZE);
    message("malloc block_7", name)
    void *block_8 = _malloc(BLOCK_SIZE);
    message("malloc block_8", name)
    void *block_9 = _malloc(BLOCK_SIZE);
    message("malloc block_9", name)
    void *block_10 = _malloc(BLOCK_SIZE);
    message("malloc block_10", name)

    debug_heap(stdout, heap);

    if (!block_1 || !block_2 || !block_3 || !block_4 || !block_5 ||
        !block_6 || !block_7 || !block_8 || !block_9 || !block_10) {
        message("failed allocation memory for initial blocks", name);
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free(block_5);
        _free(block_6);
        _free(block_7);
        _free(block_8);
        _free(block_9);
        _free(block_10);
        _free_heap(heap);
        return;
    }

    void *block_11 = _malloc(BLOCK_SIZE);
    message("try to extend heap (malloc block_11)", name);

    if (block_11 == NULL || get_header_by_pointer(block_11)->capacity.bytes != BLOCK_SIZE) {
        message("failed (new block hasn't allocated)", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free(block_5);
        _free(block_6);
        _free(block_7);
        _free(block_8);
        _free(block_9);
        _free(block_10);
        _free(block_11);
        _free_heap(heap);
        return;
    }

    const struct block_header* extended_heap = (struct block_header*) heap;
    if (size_from_capacity(extended_heap->capacity).bytes <= HEAP_SIZE) {
        message("failed (heap hasn't extended)", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free(block_5);
        _free(block_6);
        _free(block_7);
        _free(block_8);
        _free(block_9);
        _free(block_10);
        _free(block_11);
        _free_heap(heap);
        return;
    }

    message("success", name)

    _free(block_1);
    _free(block_2);
    _free(block_3);
    _free(block_4);
    _free(block_5);
    _free(block_6);
    _free(block_7);
    _free(block_8);
    _free(block_9);
    _free(block_10);
    _free(block_11);
    _free_heap(heap);

    message("ended", name)
}

void test_3() {
    const char* name = "Free one block";

    message("started", name)

    void *heap = heap_init(HEAP_SIZE);
    message("heap initialized", name)

    void *block_1 = _malloc(BLOCK_SIZE);
    message("malloc block_1", name)
    void *block_2 = _malloc(BLOCK_SIZE);
    message("malloc block_2", name)
    void *block_3 = _malloc(BLOCK_SIZE);
    message("malloc block_3", name)

    if (!block_1 || !block_2 || !block_3) {
        message("failed allocating blocks", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free_heap(heap);
        return;
    }

    _free(block_2);

    debug_heap(stdout, heap);

    if (!get_header_by_pointer(block_2)->is_free) {
        message("failed free", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free_heap(heap);
        return;
    }

    message("success", name)

    _free(block_1);
    _free(block_2);
    _free(block_3);
    _free_heap(heap);
    message("ended", name)
}

void test_4() {
    const char* name = "Free two blocks";

    message("started", name)

    void *heap = heap_init(HEAP_SIZE);
    message("heap initialized", name)

    void *block_1 = _malloc(BLOCK_SIZE);
    message("malloc block_1", name)
    void *block_2 = _malloc(BLOCK_SIZE);
    message("malloc block_2", name)
    void *block_3 = _malloc(BLOCK_SIZE);
    message("malloc block_3", name)
    void *block_4 = _malloc(BLOCK_SIZE);
    message("malloc block_4", name)

    if (!block_1 || !block_2 || !block_3 || !block_4) {
        message("failed allocating blocks", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free_heap(heap);
        return;
    }

    _free(block_2);
    _free(block_3);

    debug_heap(stdout, heap);

    if (!get_header_by_pointer(block_2)->is_free || !get_header_by_pointer(block_3)->is_free) {
        message("failed free", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free_heap(heap);
        return;
    }

    message("success", name)

    _free(block_1);
    _free(block_4);
    _free_heap(heap);
    message("ended", name)
}

void test_5() {
    const char* name = "Allocate old memory for one block";

    message("started", name)

    void *heap = heap_init(HEAP_SIZE);
    message("heap initialized", name)

    void *block_1 = _malloc(BLOCK_SIZE);
    message("malloc block_1", name)
    void *block_2 = _malloc(BLOCK_SIZE);
    message("malloc block_2", name)
    void *block_3 = _malloc(BLOCK_SIZE);
    message("malloc block_3", name)
    void *block_4 = _malloc(BLOCK_SIZE * 7);

    debug_heap(stdout, heap);

    if (!block_1 || !block_2 || !block_3 || !block_4) {
        message("failed allocating blocks", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free_heap(heap);
        return;
    }

    _free(block_2);

    debug_heap(stdout, heap);

    if (!get_header_by_pointer(block_2)->is_free) {
        message("failed free", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free_heap(heap);
        return;
    }

    void *new_block = _malloc(BLOCK_SIZE);
    message("try to allocate old memory to new block", name)

    if (!new_block) {
        message("failed allocating new block", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free(new_block);
        _free_heap(heap);
        return;
    }

    const struct block_header* extended_heap = (struct block_header*) heap;
    if (size_from_capacity(extended_heap->capacity).bytes > HEAP_SIZE) {
        message("failed to allocate old memory to new block", name);
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free(new_block);
        _free_heap(heap);
        return;
    }

    message("success", name)

    _free(block_1);
    _free(block_2);
    _free(block_3);
    _free(block_4);
    _free(new_block);

    _free_heap(heap);
    message("ended", name)
}

void test_6() {
    const char* name = "Allocate old memory for 2x-size block";

    message("started", name)

    void *heap = heap_init(HEAP_SIZE);
    message("heap initialized", name)

    void *block_1 = _malloc(BLOCK_SIZE);
    message("malloc block_1", name)
    void *block_2 = _malloc(BLOCK_SIZE);
    message("malloc block_2", name)
    void *block_3 = _malloc(BLOCK_SIZE);
    message("malloc block_3", name)
    void *block_4 = _malloc(BLOCK_SIZE);
    message("malloc block_4", name);
    void *block_5 = _malloc(BLOCK_SIZE*6);
    message("malloc block_5", name);

    debug_heap(stdout, heap);

    if (!block_1 || !block_2 || !block_3 || !block_4 || !block_5) {
        message("failed allocating blocks", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free(block_5);
        _free_heap(heap);
        return;
    }

    _free(block_2);
    _free(block_3);

    debug_heap(stdout, heap);

    if (!get_header_by_pointer(block_2)->is_free || !get_header_by_pointer(block_3)->is_free) {
        message("failed free", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free(block_5);
        _free_heap(heap);
        return;
    }

    void *new_block = _malloc(BLOCK_SIZE*2);
    message("try to allocate old memory to new block", name)

    if (!new_block) {
        message("failed allocating new block", name)
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free(block_5);
        _free(new_block);
        _free_heap(heap);
        return;
    }

    const struct block_header* extended_heap = (struct block_header*) heap;
    if (size_from_capacity(extended_heap->capacity).bytes > HEAP_SIZE) {
        message("failed to allocate old memory to new block", name);
        _free(block_1);
        _free(block_2);
        _free(block_3);
        _free(block_4);
        _free(block_5);
        _free(new_block);
        _free_heap(heap);
        return;
    }

    message("success", name)

    _free(block_1);
    _free(block_2);
    _free(block_3);
    _free(block_4);
    _free(block_5);
    _free(new_block);

    _free_heap(heap);
    message("ended", name)
}

void run_tests() {
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    test_6();
}