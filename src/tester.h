//
// Created by Kostya on 08.12.2022.
//

#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTER_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTER_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <sys/mman.h>

void run_tests();

#endif //ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTER_H
